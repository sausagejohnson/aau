var boxx = document.getElementById('boxx');
var boxxRegular = document.getElementById('boxx-regular');

var scrolled = function(e){

  var scrollY = window.scrollY | window.pageYOffset;

  if (scrollY > 80){
    boxx.className = 'bar-heading appear';
    boxxRegular.className = 'regular-heading hide';
  } else {
    boxx.className = 'bar-heading';
    boxxRegular.className = 'regular-heading';
  }
};

window.addEventListener('scroll', scrolled);
